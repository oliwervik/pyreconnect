/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

====================================================*/

import Foundation
import UIKit
import CoreLocation
import GoogleMobileAds
import Parse



// ------------------------------------------------
// MARK: - REPALCE THE STRING BELOW TO SET YOUR OWN APP NAME
// ------------------------------------------------
let APP_NAME = "Pyre Connect"



// ------------------------------------------------
// MARK: - REPLACE THE RED STRING BELOW WITH YOUR OWN BANNER UNIT ID YOU'LL GET FROM  http://apps.admob.com
// ------------------------------------------------
let ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-3940256099942544/1033173712"



// ------------------------------------------------
// MARK: - REPLACE THE STRINGS BELOW WITH YOUR OWN 'App ID' AND 'Client Key' FROM YOUR PARSE APP ON https://back4app.com
// ------------------------------------------------
let PARSE_APP_ID = "ZoiEgUFswcBb3xNec4fAgo2RZDA4TQQ1iCDXEgW6"
let PARSE_CLIENT_KEY = "xMa5HCUCPfVfKaOxXk09XMG41zc0aExVYDZMf8EE"



// ------------------------------------------------
// MARK: - EDIT THE RGBA VALUES BELOW AS YOU WISH
// ------------------------------------------------
let MAIN_COLOR = UIColor(red: 103/255, green: 93/255, blue: 252/255, alpha: 1.0)



// ------------------------------------------------
// MARK: - REPLACE THE EMAIL ADDRESS BELOW WITH THE ONE YOU WANT USERS TO USE TO CONTACT YOU
// ------------------------------------------------
let ADMIN_EMAIL_ADDRESS = "oliwervik@gmail.com"


// ------------------------------------------------
// MARK: - YOU MAY EDIT THE DISTANCE FROM YOUR CURRENT LOCATION IN THE VARIABLE BELOW (IT'S EITHER IN KM OR MILES)
// ------------------------------------------------
var  DISTANCE_FROM_YOUR_LOCATION:Float = 10




// ------------------------------------------------
// MARK: - LIST OF BUSINESS CATEGORIES
// ------------------------------------------------
let categoriesList = [
    "Company",              // company listing
    "Programmer",           // cat0
    "Network & IT",         // cat1
    "Artist",               // cat2
    "Audio & Music",        // cat3
    "Designer",             // cat4
    "Creative",             // cat5
    "Writer",              // cat6
    "Marketing",            // cat7
    "Management",           // cat8
    "Business",             // cat9
    "HR",                   // cat10
    "Communication",        // cat11
    "QA",                   // cat12
    "Producer"              // cat13
    
    // YOU CAN EDIT OR ADD CATEGORIES AS YOU WISH, JUST MAKE USRE TO ASSING THE PROPER IMAGES TO THE CATEGORIES
]










// ------------------------------------------------
// MARK: - UTILITY EXTENSIONS
// ------------------------------------------------
var hud = UIView()
var loadingCircle = UIImageView()
var toast = UILabel()

extension UIViewController {
    // ------------------------------------------------
    // MARK: - SHOW TOAST MESSAGE
    // ------------------------------------------------
    func showToast(_ message:String) {
        toast = UILabel(frame: CGRect(x: view.frame.size.width/2 - 100,
                                      y: view.frame.size.height-100,
                                      width: 200,
                                      height: 32))
        toast.font = UIFont(name: "OpenSans-Bold", size: 14)
        toast.textColor = UIColor.white
        toast.textAlignment = .center
        toast.adjustsFontSizeToFitWidth = true
        toast.text = message
        toast.layer.cornerRadius = 14
        toast.clipsToBounds = true
        toast.backgroundColor = MAIN_COLOR
        view.addSubview(toast)
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(hideToast), userInfo: nil, repeats: false)
    }
    @objc func hideToast() {
        toast.removeFromSuperview()
    }
    
    // ------------------------------------------------
    // MARK: - SHOW/HIDE LOADING HUD
    // ------------------------------------------------
    func showHUD() {
        hud.frame = CGRect(x:0, y:0,
                           width:view.frame.size.width,
                           height: view.frame.size.height)
        hud.backgroundColor = UIColor.white
        hud.alpha = 0.7
        view.addSubview(hud)
        
        loadingCircle.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        loadingCircle.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        loadingCircle.image = UIImage(named: "loading_circle")
        loadingCircle.contentMode = .scaleAspectFill
        loadingCircle.clipsToBounds = true
        animateLoadingCircle(imageView: loadingCircle, time: 0.8)
        view.addSubview(loadingCircle)
    }
    func animateLoadingCircle(imageView: UIImageView, time: Double) {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = -Double.pi * 2
        rotationAnimation.duration = time
        rotationAnimation.repeatCount = .infinity
        imageView.layer.add(rotationAnimation, forKey: nil)
    }
    func hideHUD() {
        hud.removeFromSuperview()
        loadingCircle.removeFromSuperview()
    }
    
    
    // ------------------------------------------------
    // MARK: - SHOW LOGIN ALERT
    // ------------------------------------------------
    func showLoginAlert(_ mess:String) {
        let alert = UIAlertController(title: APP_NAME,
            message: mess,
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Log in", style: .default, handler: { (action) -> Void in
            let aVC = self.storyboard?.instantiateViewController(withIdentifier: "Intro") as! Intro
            self.present(aVC, animated: true, completion: nil)
        })
        alert.addAction(ok)
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    // ------------------------------------------------
    // MARK: - FIRE A SIMPLE ALERT
    // ------------------------------------------------
    func simpleAlert(_ mess:String) {
        let alert = UIAlertController(title: APP_NAME,
                                      message: mess, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    // ------------------------------------------------
    // MARK: - SHOW ADMOB INTERSTITIAL ADS
    // ------------------------------------------------
    func showInterstitial() {
        let adMobInterstitial = GADInterstitial(adUnitID: ADMOB_INTERSTITIAL_UNIT_ID)
        adMobInterstitial.load(GADRequest())
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
            if adMobInterstitial.isReady {
                adMobInterstitial.present(fromRootViewController: self)
                print("AdMob Interstitial!")
            }})
    }
    
    // ------------------------------------------------
    // MARK: - GET PARSE IMAGE - IMAGEVIEW
    // ------------------------------------------------
    func getParseImage(object:PFObject, colName:String, imageView:UIImageView) {
        let imageFile = object[colName] as? PFFileObject
        imageFile?.getDataInBackground(block: { (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    imageView.image = UIImage(data:imageData)
                }}})
    }
    
    
    // ------------------------------------------------
    // MARK: - GET PARSE IMAGE - BUTTON
    // ------------------------------------------------
    func getParseImage(object:PFObject, colName:String, button:UIButton) {
        let imageFile = object[colName] as? PFFileObject
        imageFile?.getDataInBackground(block: { (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    button.setImage(UIImage(data:imageData), for: .normal)
        }}})
    }
    
    
    // ------------------------------------------------
    // MARK: - SAVE PARSE IMAGE
    // ------------------------------------------------
    func saveParseImage(object:PFObject, colName:String, imageView:UIImageView) {
        if imageView.image != nil {
            let imageData = imageView.image!.jpegData(compressionQuality: 1.0)
            let imageFile = PFFileObject(name:"image.jpg", data:imageData!)
            object[colName] = imageFile
        }
    }
    
    // ------------------------------------------------
    // MARK: - PROPORTIONALLY SCALE AN IMAGE TO MAX WIDTH
    // ------------------------------------------------
    func scaleImageToMaxWidth(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    // ------------------------------------------------
    // MARK: - SEND A PUSH NOTIFICATION
    // ------------------------------------------------
    func sendPushNotification(userPointer:PFUser, pushMessage:String) {
        let data = [
            "badge" : "Increment",
            "alert" : pushMessage,
            "sound" : "bingbong.aiff"
        ]
        let request = [
            "userObjectID" : userPointer.objectId!,
            "data" : data
        ] as [String : Any]
        
        PFCloud.callFunction(inBackground: "pushiOS", withParameters: request as [String : Any], block: { (results, error) in
            if error == nil { print ("\nPUSH NOTIFICATION SENT TO: \(userPointer[USER_USERNAME]!)\nMESSAGE: \(pushMessage)")
            } else { self.simpleAlert("\(error!.localizedDescription)")
        }})// ./ PFCloud
    }
    
    
    
    // ------------------------------------------------
    // MARK: - FORMAT DATE BY TIME AGO SINCE DATE
    // ------------------------------------------------
    func timeAgoSinceDate(_ date:Date, currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){ return "1 year ago"
            } else { return "Last year" }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){ return "1 month ago"
            } else { return "Last month" }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){ return "1 week ago"
            } else { return "Last week" }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){ return "1 day ago"
            } else { return "Yesterday" }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){ return "1 hour ago"
            } else { return "An hour ago" }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){ return "1 minute ago"
            } else { return "A minute ago" }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else { return "Just now" }
    }
    
    
}// ./ extension



// ------------------------------------------------
// MARK: - EXTENSION TO FORMAT LARGE NUMBERS
// ------------------------------------------------
extension Int {
    var rounded:String {
        let abbrev = "KMBTPE"
        return abbrev.enumerated().reversed().reduce(nil as String?) { accum, tuple in
            let factor = Double(self) / pow(10, Double(tuple.0 + 1) * 3)
            let format = (factor.truncatingRemainder(dividingBy: 1)  == 0 ? "%.0f%@" : "%.1f%@")
            return accum ?? (factor > 1 ? String(format: format, factor, String(tuple.1)) : nil)
            } ?? String(self)
    }
}



// ------------------------------------------------
// MARK: - GLOBAL VARIABLES
// ------------------------------------------------
var mustReload = true
var selectedCategory = ""


// -------------------------------------------------------
// MARK: - PARSE DASHBOARD CLASSES AND COLUMNS NAMES
// -------------------------------------------------------
let USER_CLASS_NAME = "_User"
let USER_USERNAME = "username"
let USER_FULLNAME = "fullName"
let USER_EMAIL = "email"
let USER_KEYWORDS = "keywords"
let USER_AVATAR = "avatar"
let USER_CATEGORY = "category"
let USER_ABOUT_ME = "aboutMe"
let USER_LOCATION = "location"
let USER_EMAIL_VERIFIED = "emailVerified"
let USER_REPORTED_BY = "reportedBy"
let USER_FAVORITED_BY = "favoritedBy"
let USER_CARD_BKG = "cardBkg"
let USER_FACEBOOK = "facebook"
let USER_TWITTER = "twitter"
let USER_YOUTUBE = "youTube"
let USER_LINKEDIN = "linkedIn"
let USER_INSTAGRAM = "instagram"
let USER_PHONE = "phone"
let USER_WEBSITE = "website"
let USER_CARD_EMAIL = "cardEmail"
let USER_HAS_MADE_CARD = "hasMadeCard"
let USER_IS_VISIBLE = "isVisible"
let USER_HAS_SEEN_NEWS = "hasSeenNews"
