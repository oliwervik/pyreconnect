/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

======================================================*/

import UIKit
import Parse

class Account: UIViewController, UITextViewDelegate {

    /*--- VIEWS ---*/
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var makeEditCardButton: UIButton!
    @IBOutlet weak var updateStatusButton: UIButton!
    @IBOutlet weak var statusTextField: UITextView!
    @IBOutlet weak var textCounter: UILabel!
    @IBOutlet weak var visibility: UISwitch!
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        // Call query
        showUserInfo()
    }
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        // Set current user
        let currentUser = PFUser.current()!
        visibility.isOn = (currentUser[USER_IS_VISIBLE] != nil)
        
        statusTextField.delegate = self
        
        // Layout
        
        updateStatusButton.layer.cornerRadius = 22
        updateStatusButton.layer.borderColor = MAIN_COLOR.cgColor
        updateStatusButton.layer.borderWidth = 2
        
        logoutButton.layer.cornerRadius = 16
        
        makeEditCardButton.layer.cornerRadius = 22
        
        avatarImg.layer.cornerRadius = avatarImg.bounds.size.width/2

    }
    // ------------------------------------------------
    // OLIWER: - LIMIT THE AMOUNT OF TEXT IN STATUS UPDATE
    // ------------------------------------------------
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
    
        var number = numberOfChars
        
        if number > 0 {
            number = (0 - number)
            number = -number
            
            number = 83 - number
        }
            
        
        
        textCounter.text = String (number)
        return numberOfChars < 84;
    }
    // ------------------------------------------------
    // MARK: - SHOW USER'S INFO
    // ------------------------------------------------
    func showUserInfo() {
        let currentUser = PFUser.current()!
        
        // Avatar
        getParseImage(object: currentUser, colName: USER_AVATAR, imageView: avatarImg)
        
        // Full Name
        fullNameLabel.text = "\(currentUser[USER_FULLNAME]!)"
        
        // Make/Edit Card Button
        let hasMadeCard = currentUser[USER_HAS_MADE_CARD] as! Bool
        if hasMadeCard { makeEditCardButton.setTitle("EDIT CARD", for: .normal)
        } else { makeEditCardButton.setTitle("+ MAKE YOUR CARD", for: .normal) }
        
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - MAKE/EDIT CARD BUTTON
    // ------------------------------------------------
    @IBAction func makeEditCardButt(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MakeEditCard") as! MakeEditCard
        present(vc, animated: true, completion: nil)
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - LOGOUT BUTTON
    // ------------------------------------------------
    @IBAction func logoutButt(_ sender: Any) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Are you sure you want to Logout?",
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Logout", style: .default, handler: { (action) -> Void in
            self.showHUD()
            
            PFUser.logOutInBackground(block: { (error) in
                if error == nil {
                    self.hideHUD()
                 
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "Intro") as! Intro
                    self.present(loginVC, animated: true, completion: nil)
            }})
        })
        alert.addAction(ok)
        
        // Cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - TERMS OF SERVICE BUTTON
    // ------------------------------------------------
    @IBAction func termsOfServiceButt(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TermsOfService") as! TermsOfService
        present(vc, animated: true, completion: nil)
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - CONTACT US BUTTON
    // ------------------------------------------------
    @IBAction func contactUsButt(_ sender: Any) {
        UIPasteboard.general.string = ADMIN_EMAIL_ADDRESS
        simpleAlert("Our email adress has been copied to your Clipboard!\nOpen your favorite Email client app and drop us a message, we'll reply to you asap.")
    }
    // ------------------------------------------------
    // OLIWER: - RECRUITING AND LOOKING FOR WORK
    // ------------------------------------------------
    @IBAction func visibilityToggle(_ sender: Any) {
            //lookingForWork.isOn.toggle()
            
        
    }
    @IBAction func updateStatus(_ sender: Any) {
        let currentUser = PFUser.current()!
        
        
        // Prepare data
        currentUser[USER_IS_VISIBLE] = visibility.isOn
        
        // Saving block
        currentUser.saveInBackground(block: { (succ, error) in
            if error == nil {
                self.hideHUD()
                self.simpleAlert("Status updated!")
                // error
            } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
            }})
    }
    

}// ./ end
