/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

===================================================*/

import UIKit
import ParseFacebookUtilsV4
import Parse


class Intro: UIViewController, CLLocationManagerDelegate {

    /*--- VIEWS ---*/
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    

    /*--- VARIABLES ---*/
    var locationManager: CLLocationManager!
    var currentLocation:CLLocation?
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        if PFUser.current() != nil { dismiss(animated: false, completion: nil) }
    }
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
        // Layouts
        appNameLabel.text = "\(APP_NAME)"
        
        facebookButton.layer.cornerRadius = 22
        signUpButton.layer.cornerRadius = 22
        signUpButton.layer.borderColor = MAIN_COLOR.cgColor
        signUpButton.layer.borderWidth = 2
        loginButton.layer.cornerRadius = 22
        loginButton.layer.borderColor = MAIN_COLOR.cgColor
        loginButton.layer.borderWidth = 2

        // Call function
        getCurrentLocation()
    }

    
    
    
    // ------------------------------------------------
    // MARK: - GET CURRENT LOCATION
    // ------------------------------------------------
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    
    
    // ------------------------------------------------
    // MARK: - CORE LOCATION DELEGATES
    // ------------------------------------------------
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Failed to get your location. You should enable Location Service in order to find local business people.",
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Enable Location Service", style: .default, handler: { (action) -> Void in
            UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!)
        })
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        currentLocation = locations.last
        print("CURRENT LOCATION: \(currentLocation!.coordinate.latitude)")
    }
    

    
    
    
    // ------------------------------------------------
    // MARK: - FACEBOOK LOGIN BUTTON
    // ------------------------------------------------
    @IBAction func facebookButt(_ sender: Any) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Do you agree with our Terms of Service?",
            preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            // Set permissions required from the facebook user account
            let permissions = ["public_profile", "email"];
            self.showHUD()
            
            // LOGIN WITH FACEBOOK
            PFFacebookUtils.logInInBackground(withReadPermissions: permissions) { (user, error) in
                if user == nil {
                    self.hideHUD()
                    self.simpleAlert("Facebook login cancelled.")
                    
                } else if (user!.isNew) {
                    print("NEW USER signed up or logged in with Facebook");
                    self.getFacebookUserData()
                    
                } else {
                    print("OLD USER logged in with Facebook!");
                    self.hideHUD()
                    self.dismiss(animated: true, completion: nil)
                }}
        })
        alert.addAction(yes)

        
        // TERMS OF SERVICE
        let tos = UIAlertAction(title: "Read Terms of Service", style: .default, handler: { (action) -> Void in
            self.tosButt(self)
        })
        alert.addAction(tos)

        
        // CANCEL BUTTON
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // ------------------------------------------------
    // MARK: - GET FACEBOOK USER'S DATA
    // ------------------------------------------------
    func getFacebookUserData() {
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, picture.type(large)"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest) { (connection, result, error) in
                if error == nil {
                    let userData:[String:AnyObject] = result as! [String : AnyObject]
     
                    let currentUser = PFUser.current()!
                    
                    // Get data
                    let facebookID = userData["id"] as! String
                    let name = userData["name"] as! String
                    var email = ""
                    if userData["email"] != nil { email = userData["email"] as! String
                    } else { email = "\(facebookID)@facebook.com" }
                    
                    // Get profile picture
                    let pictureURL = URL(string: "https://graph.facebook.com/\(facebookID)/picture?type=large")
                    let urlRequest = URLRequest(url: pictureURL!)
                    let session = URLSession.shared
                    let dataTask = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                        if error == nil && data != nil {
                            let image = UIImage(data: data!)
                            let imageData = image!.jpegData(compressionQuality: 0.8)
                            let imageFile = PFFileObject(name:"avatar.jpg", data:imageData!)
                            currentUser[USER_AVATAR] = imageFile
                            currentUser.saveInBackground(block: { (succ, error) in
                                print("...AVATAR SAVED!")
                                
                                self.hideHUD()
                                self.dismiss(animated: true, completion: nil)
                            })
                        // error
                        } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
                    }})
                    dataTask.resume()
                    
                    
                    // Update user data
                    let nameArr = name.components(separatedBy: " ")
                    var username = String()
                    for word in nameArr {
                        username.append(word.lowercased())
                    }
                    currentUser.username = username
                    currentUser.email = email
                    currentUser[USER_FULLNAME] = name
                    currentUser[USER_REPORTED_BY] = [String]()
                    currentUser[USER_FAVORITED_BY] = [String]()
                    currentUser[USER_HAS_MADE_CARD] = false
                    let gp = PFGeoPoint(location: self.currentLocation)
                    currentUser[USER_LOCATION] = gp
                    
                    currentUser.saveInBackground(block: { (succ, error) in
                        if error == nil {
                            print("USER'S DATA UPDATED...")
                    }})
                    
                // error
                } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription))")
            }}
            connection.start()
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - SING UP BUTTON
    // ------------------------------------------------
    @IBAction func signUpButt(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "SignUp") as! SignUp
        present(aVC, animated: true, completion: nil)
    }
    
    
    
    // ------------------------------------------------
    // MARK: - LOGIN BUTTON
    // ------------------------------------------------
    @IBAction func loginButt(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(aVC, animated: true, completion: nil)
    }
    
    
    // ------------------------------------------------
    // MARK: - TERMS OF SERVICE BUTTON
    // ------------------------------------------------
    @IBAction func tosButt(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "TermsOfService") as! TermsOfService
        present(aVC, animated: true, completion: nil)
    }
    
    
    
}// ./ end
