//
//  Chat.swift
//  PyreConnect
//
//  Created by Oliwer Svensson on 2019-06-23.
//  Copyright © 2019 xscoder. All rights reserved.
//

import UIKit
import Parse
import PusherChatkit

class Chat: UIViewController {
    
    var chatManager: ChatManager!
    var currentUser: PCCurrentUser?
    var chatManagerDelegate: PCChatManagerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatKitConnect()
    }
        
        public func chatKitConnect()
        {
            chatManager = ChatManager(
                instanceLocator: "v1:us1:636d000f-a858-4d4d-a9a6-4852cd702d2d",
                tokenProvider: PCTokenProvider(url: "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/636d000f-a858-4d4d-a9a6-4852cd702d2d/token"),
                userID: "oliwervik"
            )
            
            self.chatManagerDelegate = MyChatManagerDelegate()
            
            chatManager.connect(
                delegate: self.chatManagerDelegate!
            ) { [unowned self] currentUser, error in
                guard error == nil else {
                    print("Error connecting: \(error!.localizedDescription)")
                    return
                }
                print("Connected!")
                
                guard let currentUser = currentUser else {
                    print("CurrentUser object is nil")
                    return
                }
                self.currentUser = currentUser
                
                currentUser.subscribeToRoomMultipart(room: currentUser.rooms[0], roomDelegate: self) { err in
                    guard error == nil else {
                        print("Error subscribing to room: \(error!.localizedDescription)")
                        return
                    }
                    print("Subscribed to room!")
                }
            }
            // Do any additional setup after loading the view.

            
        }
        }

extension UIViewController: PCRoomDelegate {
    public func onMultipartMessage(_ message: PCMultipartMessage) {
        print("\(message.sender.id) sent message with parts \(message.parts)")
    }
}
    
    class MyChatManagerDelegate: PCChatManagerDelegate {}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


