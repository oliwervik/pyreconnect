/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

=====================================================*/

import UIKit

class CardCell: UITableViewCell {

    /*--- VIEWS ---*/
    @IBOutlet weak var cardBkg: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var socialButtonsView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var messageButtonCards: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Layouts
        avatarImg.layer.cornerRadius = avatarImg.bounds.size.width/2
        cardView.layer.cornerRadius = 24
    }

}
