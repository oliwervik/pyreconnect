/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

=====================================================*/

import UIKit

class CategoryCell: UITableViewCell {

    /*--- VIEWS ---*/
    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var catLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Layouts
        catImage.layer.cornerRadius = 10
    }

}
