/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

=====================================================*/

import UIKit
import Parse
import GoogleMobileAds
import MessageUI


class CardDetails: UIViewController, MFMailComposeViewControllerDelegate {
    
    /*--- VIEWS ---*/
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var aboutTxt: UITextView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var socialButtonsView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    
    
    /*--- VARIABLES ---*/
    var cardObj = PFUser(className: USER_CLASS_NAME)
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
        // Full name
        titleLabel.text = "\(cardObj[USER_FULLNAME]!)".uppercased()
        
        
        // Layout
        avatarImg.layer.cornerRadius = avatarImg.bounds.size.width/2
        callButton.layer.cornerRadius = 22
        emailButton.layer.cornerRadius = 22
        websiteButton.layer.cornerRadius = 22

      
        // Call query
        showCardInfo()
    }

    
    
    // ------------------------------------------------
    // MARK: - SHOW USER'S INFO
    // ------------------------------------------------
    func showCardInfo() {
        
        // Avatar
        getParseImage(object: cardObj, colName: USER_AVATAR, imageView: avatarImg)
        
        // Category
        categoryLabel.text = "\(cardObj[USER_CATEGORY]!)"
        
        // About
        aboutTxt.text = "\(cardObj[USER_ABOUT_ME]!)"
        aboutTxt.sizeToFit()
        
        // Resize Views
        bottomView.frame.origin.y = aboutTxt.frame.size.height + aboutTxt.frame.origin.y + 20
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
                                                 height: bottomView.frame.origin.y + bottomView.frame.size.height)
        
        // Social Buttons
        var socialButtonsArr = [UIButton]()
        var X:CGFloat = 0
        let W:CGFloat = 32
        let G:CGFloat = 2
        
        if cardObj[USER_FACEBOOK] != nil {
            if cardObj[USER_FACEBOOK] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "fb_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_FACEBOOK]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        if cardObj[USER_TWITTER] != nil {
            if cardObj[USER_TWITTER] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "tw_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_TWITTER]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        if cardObj[USER_INSTAGRAM] != nil {
            if cardObj[USER_INSTAGRAM] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "inst_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_INSTAGRAM]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        if cardObj[USER_YOUTUBE] != nil {
            if cardObj[USER_YOUTUBE] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "yt_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_YOUTUBE]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        if cardObj[USER_LINKEDIN] != nil {
            if cardObj[USER_LINKEDIN] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "lin_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_LINKEDIN]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        // Add Buttons in View
        for i in 0..<socialButtonsArr.count {
            let butt = socialButtonsArr[i]
            butt.frame = CGRect(x: X, y: 0, width: W, height: 32)
            X += W + G
            socialButtonsView.addSubview(butt)
        }
        
        
        // Call Button
        callButton.titleLabel?.text = "Call \(cardObj[USER_FULLNAME]!)".uppercased()
        
        // Email Button
        emailButton.titleLabel?.text = "Email \(cardObj[USER_FULLNAME]!)".uppercased()
        
        // Websiste Button
        if cardObj[USER_WEBSITE] != nil {
            let website = "\(cardObj[USER_WEBSITE]!)"
            if website != "" { websiteButton.isHidden = false
            } else { websiteButton.isHidden = true }
        } else { websiteButton.isHidden = true }
        
    }
    

    
    
    // ------------------------------------------------
    // MARK: - SOCIAL BUTTONS
    // ------------------------------------------------
    @objc func socialButt(_ sender: UIButton) {
        let aURL = URL(string: sender.titleLabel!.text!)
        UIApplication.shared.openURL(aURL!)
    }
    
    
    // ------------------------------------------------
    // MARK: - CALL BUTTON
    // ------------------------------------------------
    @IBAction func callButt(_ sender: Any) {
        let phoneStr = "\(cardObj[USER_PHONE]!)"
        let aURL = URL(string: "telprompt://\(phoneStr)")!
        if UIApplication.shared.canOpenURL(aURL) { UIApplication.shared.openURL(aURL) }
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - EMAIL BUTTON
    // ------------------------------------------------
    @IBAction func emailButt(_ sender: Any) {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setToRecipients(["\(cardObj[USER_CARD_EMAIL]!)"])
        
        if MFMailComposeViewController.canSendMail() { present(mailComposer, animated: true, completion: nil)
        } else { simpleAlert("Your device cannot send emails. Please configure an email address into Settings -> Mail, Contacts, Calendars.") }
    }
    
    
    // ------------------------------------------------
    // MARK: - EMAIL DELEGATE
    // ------------------------------------------------
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        var resultMess = ""
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            resultMess = "Mail cancelled"
        case MFMailComposeResult.saved.rawValue:
            resultMess = "Mail saved"
        case MFMailComposeResult.sent.rawValue:
            resultMess = "Your email has been sent!"
        case MFMailComposeResult.failed.rawValue:
            resultMess = "Something went wrong with sending your email, try again later."
        default:break
        }
        
        // Show email result alert
        simpleAlert(resultMess)
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: -   WEBSITE BUTTON
    // ------------------------------------------------
    @IBAction func websiteButt(_ sender: Any) {
        let aURL = URL(string: "\(cardObj[USER_WEBSITE]!)")
        UIApplication.shared.openURL(aURL!)
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - REPORT BUTTON
    // ------------------------------------------------
    @IBAction func reportButt(_ sender: Any) {
        let currentUser = PFUser.current()!
        
        let alert = UIAlertController(title: APP_NAME,
            message: "Are you sure you want to report this Card to the Admin as inappropriate?",
            preferredStyle: .alert)
        
        // Report user
        let report = UIAlertAction(title: "Report '\(cardObj[USER_FULLNAME]!)'", style: .default, handler: { (action) -> Void in
            var reportedBy = self.cardObj[USER_REPORTED_BY] as! [String]
            reportedBy.append(currentUser.objectId!)
            self.showHUD()
            
            let request = [
                "userId" : self.cardObj.objectId!,
                "reportedBy" : reportedBy
            ] as [String : Any]
            PFCloud.callFunction(inBackground: "reportUser", withParameters: request as [String : Any], block: { (results, error) in
                if error == nil {
                    self.hideHUD()
                    
                    // Fire alert
                    let alert = UIAlertController(title: APP_NAME,
                        message: "Thanks for reporting this Card. We'll take action for it within 24h.",
                        preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        mustReload = true
                        self.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                // error
                } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
            }})// ./ PFCloud
        })
        alert.addAction(report)
        
        
        // Cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - BACK BUTTON
    // ------------------------------------------------
    @IBAction func backButt(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
   
}// ./ end
