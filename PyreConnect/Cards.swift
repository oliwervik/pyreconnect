/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

=====================================================*/

import UIKit
import Parse
import GoogleMobileAds

class Cards: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    /*--- VIEWS ---*/
    @IBOutlet weak var cardsTableView: UITableView!
    
    
    /*--- VARIABLES ---*/
    var cardsArray = [PFUser]()
    var categoryName = ""
    var keywordsStr = ""
    var locationManager: CLLocationManager!
    var currentLocation:CLLocation?
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        let user = PFUser()
        
        //check if user is visible before querying cards
        if user[USER_IS_VISIBLE] as? Bool != true
        {
            //null
        }
        
        if mustReload {
            mustReload = false
            // Call query
            queryCards()
        }
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
        print("CATEGORY: \(categoryName)")
        print("KEYWORDS STR: \(keywordsStr)")

        // Layout
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            cardsTableView.frame.size.width = 400
            cardsTableView.center.x = view.center.x
        }

        
        // Call function
        getCurrentLocation()
        
        
        // Show interstitial ads
        showInterstitial()
    }

    
    
    
    // ------------------------------------------------
    // MARK: - GET CURRENT LOCATION
    // ------------------------------------------------
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    
    
    // ------------------------------------------------
    // MARK: - CORE LOCATION DELEGATES
    // ------------------------------------------------
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Failed to get your location. You should enable Location Service in order to find local business people.",
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Enable Location Service", style: .default, handler: { (action) -> Void in
            UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!)
        })
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        currentLocation = locations.last
        print("CURRENT LOCATION: \(currentLocation!.coordinate.latitude)")
        
        // Call query
        queryCards()
    }
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - QUERY CARDS
    // ------------------------------------------------
    func queryCards() {
        showHUD()
        let currentUser  = PFUser.current()!

        let query = PFUser.query()!
        
        query.whereKey(USER_HAS_MADE_CARD, equalTo: true)
        if categoryName != "" { query.whereKey(USER_CATEGORY, contains: categoryName) }
        if keywordsStr != "" {
            let keywords = keywordsStr.components(separatedBy: " ")
            query.whereKey(USER_KEYWORDS, containedIn: keywords)
        }
        
        let gp = PFGeoPoint(location: currentLocation!)
        
        query.whereKey(USER_LOCATION, nearGeoPoint: gp, withinKilometers: Double(DISTANCE_FROM_YOUR_LOCATION)) // <-- REPLACE 'withinKilometers' WITH 'withinMiles' IN CASE YOU WANT DISTANCE IN MILES
        
        query.whereKey(USER_REPORTED_BY, notContainedIn: [currentUser.objectId!])
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                self.hideHUD()
                self.cardsArray = objects! as! [PFUser]
                self.cardsTableView.reloadData()
            // error
            } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
        }}
    }
    
    
    
    // ------------------------------------------------
    // MARK: - SHOW DATA IN TABLEVIEW
    // ------------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! CardCell
        
        // Parse Obj
        var cardObj = PFUser()
        cardObj = cardsArray[indexPath.row]
        
        // currentUser
        let currentUser = PFUser.current()!
        
        
        // Avatar
        getParseImage(object: cardObj, colName: USER_AVATAR, imageView: cell.avatarImg)
        
        // Full name
        cell.fullNameLabel.text = "\(cardObj[USER_FULLNAME]!)".uppercased()
        
        // Card Bkg
        cell.cardBkg.image = UIImage(named: "\(cardObj[USER_CARD_BKG]!)")
        
        // About
        cell.aboutLabel.text = "\(cardObj[USER_ABOUT_ME]!)"
        
        // Category
        cell.categoryLabel.text = "\(cardObj[USER_CATEGORY]!)"
        
        
        // Social Buttons
        var socialButtonsArr = [UIButton]()
        for view in cell.socialButtonsView.subviews { view.removeFromSuperview() }
        var X:CGFloat = 0
        let W:CGFloat = 32
        let G:CGFloat = 2
        
        if cardObj[USER_FACEBOOK] != nil {
            if cardObj[USER_FACEBOOK] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "fb_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_FACEBOOK]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
            
        if cardObj[USER_TWITTER] != nil {
            if cardObj[USER_TWITTER] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "tw_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_TWITTER]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }
        
        if cardObj[USER_INSTAGRAM] != nil {
            if cardObj[USER_INSTAGRAM] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "inst_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_INSTAGRAM]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }

        if cardObj[USER_YOUTUBE] != nil {
            if cardObj[USER_YOUTUBE] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "yt_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_YOUTUBE]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }

        if cardObj[USER_LINKEDIN] != nil {
            if cardObj[USER_LINKEDIN] as! String != "" {
                let butt = UIButton(type: .custom)
                butt.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                butt.setBackgroundImage(UIImage(named: "lin_butt"), for: .normal)
                butt.setTitle("\(cardObj[USER_LINKEDIN]!)", for: .normal)
                butt.setTitleColor(UIColor.clear, for: .normal)
                butt.addTarget(self, action: #selector(socialButt(_:)), for: .touchUpInside)
                socialButtonsArr.append(butt)
            }
        }

        // Add Buttons in View
        for i in 0..<socialButtonsArr.count {
            let butt = socialButtonsArr[i]
            butt.frame = CGRect(x: X, y: 0, width: W, height: 32)
            X += W + G
            cell.socialButtonsView.addSubview(butt)
        }

        
        
        // Favorite Button
        let favoritedBy = cardObj[USER_FAVORITED_BY] as! [String]
        if favoritedBy.contains(currentUser.objectId!) {
            cell.favoriteButton.setBackgroundImage(UIImage(named: "favorited_butt"), for: .normal)
        } else {
            cell.favoriteButton.setBackgroundImage(UIImage(named: "favorite_butt"), for: .normal)
        }
        
        
        // Tags
        cell.viewButton.tag = indexPath.row
        cell.callButton.tag = indexPath.row
        cell.favoriteButton.tag = indexPath.row
        cell.messageButtonCards.tag = indexPath.row

    return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 412
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - SOCIAL BUTTONS
    // ------------------------------------------------
    @objc func socialButt(_ sender: UIButton) {
        let aURL = URL(string: sender.titleLabel!.text!)
        UIApplication.shared.openURL(aURL!)
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - CALL BUTTON
    // ------------------------------------------------
    @IBAction func callButt(_ sender: UIButton) {
        // Parse Obj
        var cardObj = PFUser()
        cardObj = cardsArray[sender.tag]
        
        let phoneStr = "\(cardObj[USER_PHONE]!)"
        let aURL = URL(string: "telprompt://\(phoneStr)")!
        if UIApplication.shared.canOpenURL(aURL) { UIApplication.shared.openURL(aURL) }
    }
    
    
    
    // ------------------------------------------------
    // MARK: - MESSAGE BUTTON
    // ------------------------------------------------
    
    @IBAction func messageButt(_ sender: UIButton) {
        var cardObj = PFUser()
        cardObj = cardsArray[sender.tag]
        let cardName = cardObj[USER_FULLNAME] as! String
        
        let uid = PFUser.current()!.objectId!
        let selectedId = cardObj.objectId!
        let conversationId = selectedId<uid ? selectedId+uid : uid+selectedId
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatViewController
        navigationController?.pushViewController(vc, animated: true)
        vc.conversationId = conversationId
        vc.title = cardName.uppercased()
        vc.self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    // ------------------------------------------------
    // MARK: - FAVORITE BUTTON
    // ------------------------------------------------
    @IBAction func favoriteButt(_ sender: UIButton) {
        // Parse Obj
        var cardObj = PFUser()
        cardObj = cardsArray[sender.tag]
        
        let currentUser = PFUser.current()!
        var favoritedBy = cardObj[USER_FAVORITED_BY] as! [String]
            
        // UN-FAVORITE CARD
        if favoritedBy.contains(currentUser.objectId!) {
            favoritedBy = favoritedBy.filter{$0 != currentUser.objectId! }
                
            let request = [
                "userId" : cardObj.objectId!,
                "favoritedBy" : favoritedBy
            ] as [String : Any]
            PFCloud.callFunction(inBackground: "favoriteUser", withParameters: request as [String : Any], block: { (results, error) in
                if error == nil {
                    sender.setBackgroundImage(UIImage(named: "favorite_butt"), for: .normal)
                // error
                } else { self.simpleAlert("\(error!.localizedDescription)")
            }})// ./ PFCloud
                
                
        // FAVORITE CARD
        } else {
            favoritedBy.append(currentUser.objectId!)
                
            let request = [
                "userId" : cardObj.objectId!,
                "favoritedBy" : favoritedBy
            ] as [String : Any]
            PFCloud.callFunction(inBackground: "favoriteUser", withParameters: request as [String : Any], block: { (results, error) in
                if error == nil {
                    sender.setBackgroundImage(UIImage(named: "favorited_butt"), for: .normal)
                // error
                } else { self.simpleAlert("\(error!.localizedDescription)")
            }})// ./ PFCloud
                
        }// ./ If
    }
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW CARD BUTTON
    // ------------------------------------------------
    @IBAction func viewCardButt(_ sender: UIButton) {
        // Parse Obj
        var cardObj = PFUser()
        cardObj = cardsArray[sender.tag]
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CardDetails") as! CardDetails
        vc.cardObj = cardObj
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    

    
    
    // ------------------------------------------------
    // MARK: - BACK BUTTON
    // ------------------------------------------------
    @IBAction func backButt(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
   
}// ./ end
