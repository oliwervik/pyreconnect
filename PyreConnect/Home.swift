/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

=====================================================*/

import UIKit
import Parse
import GoogleMobileAds

class Home: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, CLLocationManagerDelegate {
    
    /*--- VIEWS ---*/
    @IBOutlet weak var categoriesTableView: UITableView!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var filtersButton: UIButton!
    
    
    /*--- VARIABLES ---*/
    var locationManager: CLLocationManager!

    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        // currentUser IS LOGGED IN!
        if PFUser.current() != nil {
            // Call function
            getCurrentLocation()
            
        // currentUser IS NOT LOGGED IN...
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Intro") as! Intro
            present(vc, animated: true, completion: nil)
        }
        
        //init user
        let currentUser = PFUser.current()
        
        
        // CHECK IF USER HAS SEEN NEWS
        if (currentUser?[USER_HAS_SEEN_NEWS] as? Bool != true) {
            // Call function
            let vcLanding = storyboard?.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
            present(vcLanding, animated: true, completion: nil)
            
        } else {
            //null
        }
        
        if (currentUser?[USER_HAS_SEEN_NEWS] == nil) {
            // Call function
            let vcLanding = storyboard?.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
            present(vcLanding, animated: true, completion: nil)
            
        } else {
            //null
        }
        
    }

    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
        // Layout
        searchTxt.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: searchTxt.frame.height))
        searchTxt.leftViewMode = .always
        searchTxt.layer.cornerRadius = 6
        
        
        
        // Show interstitial ads
        showInterstitial()
    }

    
    
    
    // ------------------------------------------------
    // MARK: - GET CURRENT LOCATION
    // ------------------------------------------------
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    
    
    // ------------------------------------------------
    // MARK: - CORE LOCATION DELEGATES
    // ------------------------------------------------
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Failed to get your location. You should enable Location Service in order to find local business people.",
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Enable Location Service", style: .default, handler: { (action) -> Void in
            UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!)
        })
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        let location = locations.last

        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location!, completionHandler: { (placemarks, error) -> Void in
            let placeArray:[CLPlacemark] = placemarks!
            var placemark: CLPlacemark!
            placemark = placeArray[0]
            
            // City
            let city = placemark.addressDictionary?["City"] as? String ?? ""
            
            // SearchTxt placeholder
            self.searchTxt.placeholder = "Search in \(city)"
        })
    }
    

    
    
    
    // ------------------------------------------------
    // MARK: - DISTANCE SLIDER CHANGED
    // ------------------------------------------------
    @IBAction func distanceSliderChanged(_ sender: UISlider) {
        DISTANCE_FROM_YOUR_LOCATION = sender.value
        distanceLabel.text = String(format: "%.0f", DISTANCE_FROM_YOUR_LOCATION) + " Km" // replace 'Km' with 'Miles' if you want to set distance in Miles
    }
    
    // ------------------------------------------------
    // OLIWER: - OPEN FILTERS MENU
    // ------------------------------------------------
    
    @IBAction func filterTouchUp(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Filters") as! filtersView
        present(vc, animated: true, completion: nil)
    }
    
    
    // ------------------------------------------------
    // MARK: - SHOW DATA IN TABLEVIEW
    // ------------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        // Name
        cell.catLabel.text = "\(categoriesList[indexPath.row])"
        
        // Image
        cell.catImage.image = UIImage(named: "cat\(indexPath.row)")
        
    return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    
    // ------------------------------------------------
    // MARK: - SELECT A CATEGORY
    // ------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Cards") as! Cards
        vc.categoryName = "\(categoriesList[indexPath.row])"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    // ------------------------------------------------
    // MARK: - DISMISS KEYBOARD ON SCROLL DOWN
    // ------------------------------------------------
    var lastContentOffset: CGFloat = 0
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            searchTxt.resignFirstResponder()
            searchTxt.text = ""
        }
    }
    
    
    
    // ------------------------------------------------
    // MARK: - SEARCH BY KEYWORDS - TEXTFIELD DELEGATE
    // ------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Cards") as! Cards
            vc.keywordsStr = textField.text!.lowercased()
            navigationController?.pushViewController(vc, animated: true)
        }
    return true
    }
    
    
}// ./ end
