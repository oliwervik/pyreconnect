/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

======================================================*/

import UIKit
import Parse

class MakeEditCard: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    /*--- VIEWS ---*/
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var cardBkgImg: UIImageView!
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var fullNameTxt: UITextField!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var aboutMeTxt: UITextView!
    @IBOutlet weak var backgroundsScrollView: UIScrollView!
    @IBOutlet weak var facebookTxt: UITextField!
    @IBOutlet weak var twitterTxt: UITextField!
    @IBOutlet weak var youtubeTxt: UITextField!
    @IBOutlet weak var linkedinTxt: UITextField!
    @IBOutlet weak var instagramTxt: UITextField!
    @IBOutlet weak var websiteTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var saveCardButton: UIButton!
    @IBOutlet weak var removeCardButton: UIButton!
    
    
    /*--- VARIABLES ---*/
    var textFields = [UITextField]()
    var selectedBkg = "Profile1"
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        if selectedCategory != "" {
            categoryButton.setTitle("   " + selectedCategory, for: .normal)
        }
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
        // Layout
        avatarButton.layer.cornerRadius = avatarButton.bounds.size.width/2
        avatarButton.imageView?.contentMode = .scaleAspectFill
        avatarButton.clipsToBounds = true
        categoryButton.layer.cornerRadius = 6
        aboutMeTxt.layer.cornerRadius = 6
        backgroundsScrollView.layer.cornerRadius = 6
        backgroundsScrollView.contentSize = CGSize(width: 130*6, height: 111)
        saveCardButton.layer.cornerRadius = 22
        removeCardButton.layer.cornerRadius = 22

        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
                                                 height: 1800)
        
        
        
        textFields.append(fullNameTxt)
        textFields.append(facebookTxt)
        textFields.append(twitterTxt)
        textFields.append(youtubeTxt)
        textFields.append(linkedinTxt)
        textFields.append(instagramTxt)
        textFields.append(websiteTxt)
        textFields.append(phoneTxt)
        textFields.append(emailTxt)

        for txt in textFields {
            txt.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txt.frame.height))
            txt.leftViewMode = .always
            txt.layer.cornerRadius = 6
        }
        
        
        
        // Make or Edit Card
        let currentUser = PFUser.current()!
        let hasMadeCard = currentUser[USER_HAS_MADE_CARD] as! Bool
        if hasMadeCard {
            showUserInfo()
            titleLabel.text = "Edit Card"
            removeCardButton.isHidden = false
        } else {
            removeCardButton.isHidden = true
            titleLabel.text = "Make Your Card"
        }
        
        
        // Full Name
        fullNameTxt.text = "\(currentUser[USER_FULLNAME]!)"
        
        // Avatar
        getParseImage(object: currentUser, colName: USER_AVATAR, button: avatarButton)
    }

    
    
    
    
    // ------------------------------------------------
    // MARK: - SHOW USER'S INFO
    // ------------------------------------------------
    func showUserInfo() {
        let currentUser = PFUser.current()!
        
        // Category
        categoryButton.setTitle("   \(currentUser[USER_CATEGORY]!)", for: .normal)
        selectedCategory = "\(currentUser[USER_CATEGORY]!)"
       
        // Card Bkg
        cardBkgImg.image = UIImage(named: "\(currentUser[USER_CARD_BKG]!)")
        selectedBkg = "\(currentUser[USER_CARD_BKG]!)"
        
        // About me
        aboutMeTxt.text = "\(currentUser[USER_ABOUT_ME]!)"
        
        // Facebook
        facebookTxt.text = "\(currentUser[USER_FACEBOOK]!)"
        
        // Twitter
        twitterTxt.text = "\(currentUser[USER_TWITTER]!)"

        // YouTube
        youtubeTxt.text = "\(currentUser[USER_YOUTUBE]!)"

        // Linked In
        linkedinTxt.text = "\(currentUser[USER_LINKEDIN]!)"

        // Instagram
        instagramTxt.text = "\(currentUser[USER_INSTAGRAM]!)"

        // Website
        websiteTxt.text = "\(currentUser[USER_WEBSITE]!)"

        // Phone
        phoneTxt.text = "\(currentUser[USER_PHONE]!)"
        
        // Card Email
        emailTxt.text = "\(currentUser[USER_CARD_EMAIL]!)"
        
    }
    
    
    
    // ------------------------------------------------
    // MARK: - CHANGE AVATAR PHOTO BUTTON
    // ------------------------------------------------
    @IBAction func avatarButt(_ sender: Any) {
        dismissKeyboard()
        
        // Fire alert
        let alert = UIAlertController(title: APP_NAME,
            message: "Select source",
            preferredStyle: .alert)
        
        // Camera
        let camera = UIAlertAction(title: "Take a photo", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(camera)

        // Photo Library
        let library = UIAlertAction(title: "Pick from library", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(library)

        // Cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // ------------------------------------------------
    // MARK: - IMAGE PICKER DELEGATE
    // ------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            avatarButton.setImage(scaleImageToMaxWidth(image: image, newWidth: 300), for: .normal)
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - CHOOSE CATEGORY BUTTON
    // ------------------------------------------------
    @IBAction func chooseCategoryButt(_ sender: Any) {
        dismissKeyboard()
        let vc = storyboard?.instantiateViewController(withIdentifier: "Categories") as! Categories
        present(vc, animated: true, completion: nil)
    }
    
    
    
    // ------------------------------------------------
    // MARK: - CARD BACKGROUND BUTTONS
    // ------------------------------------------------
    @IBAction func bkgButt(_ sender: UIButton) {
        dismissKeyboard()
        selectedBkg = "Profile\(sender.tag)"
        cardBkgImg.image = UIImage(named: selectedBkg)
    }
    
    
    
    // ------------------------------------------------
    // MARK: - SAVE CARD BUTTON
    // ------------------------------------------------
    @IBAction func saveCardButt(_ sender: Any) {
        if fullNameTxt.text == "" || selectedCategory == "" || aboutMeTxt.text == "" || phoneTxt.text == "" || emailTxt.text == "" {
            simpleAlert("You must set the following info to save your Card:\n• Name\n• Your Job Category\n• Something about you\n• Your Phone Nr.\n• Your Email")
            
        } else {
            showHUD()
            let currentUser = PFUser.current()!
            dismissKeyboard()
            
            // Prepare data
            currentUser[USER_HAS_MADE_CARD] = true
            currentUser[USER_FULLNAME] = fullNameTxt.text!
            currentUser[USER_CATEGORY] = selectedCategory
            currentUser[USER_CARD_BKG] = selectedBkg
            currentUser[USER_ABOUT_ME] = aboutMeTxt.text!
            currentUser[USER_FACEBOOK] = facebookTxt.text!
            currentUser[USER_TWITTER] = twitterTxt.text!
            currentUser[USER_YOUTUBE] = youtubeTxt.text!
            currentUser[USER_LINKEDIN] = linkedinTxt.text!
            currentUser[USER_INSTAGRAM] = instagramTxt.text!
            currentUser[USER_WEBSITE] = websiteTxt.text!
            currentUser[USER_PHONE] = phoneTxt.text!
            currentUser[USER_CARD_EMAIL] = emailTxt.text!
            
            // Make keywords
            let k1 = fullNameTxt.text!.lowercased().components(separatedBy: " ")
            let k2 = aboutMeTxt.text.lowercased().components(separatedBy: " ")
            let keywords = k1 + k2
            currentUser[USER_KEYWORDS] = keywords
            
            // Avatar
            let imageData = avatarButton.imageView!.image!.jpegData(compressionQuality: 1)
            let imageFile = PFFileObject(name:"avatar.jpg", data:imageData!)
            currentUser[USER_AVATAR] = imageFile
            
            
            // Saving block
            currentUser.saveInBackground(block: { (succ, error) in
                if error == nil {
                    selectedCategory = ""
                    self.hideHUD()
                    self.simpleAlert("Your Card has been saved!")
                // error
                } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
            }})
        }// ./ If
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - REMOVE CARD BUTTON
    // ------------------------------------------------
    @IBAction func removeCardButt(_ sender: Any) {
        let currentUser = PFUser.current()!
        
        // Fire alert
        let alert = UIAlertController(title: APP_NAME,
            message: "Are you sure you want to remove your Card?",
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Remove Card", style: .default, handler: { (action) -> Void in
            currentUser[USER_HAS_MADE_CARD] = false
            currentUser.saveInBackground()
            self.simpleAlert("Your Card is no longer visible in this App!")
            self.saveCardButton.isHidden = true
        })
        alert.addAction(ok)
        
        // Cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    // ------------------------------------------------
    // MARK: - TEXTFIELD DELEGATES
    // ------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fullNameTxt { fullNameTxt.resignFirstResponder() }
        if textField == facebookTxt { facebookTxt.resignFirstResponder() }
        if textField == twitterTxt { twitterTxt.resignFirstResponder() }
        if textField == youtubeTxt { youtubeTxt.resignFirstResponder() }
        if textField == linkedinTxt { linkedinTxt.resignFirstResponder() }
        if textField == instagramTxt { instagramTxt.resignFirstResponder() }
        if textField == websiteTxt { websiteTxt.resignFirstResponder() }
        if textField == phoneTxt { phoneTxt.resignFirstResponder() }
        if textField == emailTxt { emailTxt.resignFirstResponder() }

    return true
    }
    
    
    
    // ------------------------------------------------
    // MARK: - DISMISS KEYBOARD
    // ------------------------------------------------
    func dismissKeyboard() {
        fullNameTxt.resignFirstResponder()
        facebookTxt.resignFirstResponder()
        twitterTxt.resignFirstResponder()
        youtubeTxt.resignFirstResponder()
        linkedinTxt.resignFirstResponder()
        instagramTxt.resignFirstResponder()
        websiteTxt.resignFirstResponder()
        phoneTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
    }
    
    
    // ------------------------------------------------
    // MARK: - DISMISS BUTTON
    // ------------------------------------------------
    @IBAction func dismissButt(_ sender: Any) {
        dismissKeyboard()
        selectedCategory = ""
        dismiss(animated: true, completion: nil)
    }
    
    
}// ./ end
