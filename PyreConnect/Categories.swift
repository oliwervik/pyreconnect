/*==================================================
 bCards
 
 © XScoder 2018
 All Rights reserved
 
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED

 ====================================================*/

import UIKit
import Parse
import GoogleMobileAds

class Categories: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /*--- VIEWS ---*/
    @IBOutlet weak var categoriesTableView: UITableView!
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
            super.viewDidLoad()
        
    
    }

    
    
    // ------------------------------------------------
    // MARK: - SHOW DATA IN TABLEVIEW
    // ------------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        // Name
        cell.catLabel.text = "\(categoriesList[indexPath.row])"
        
        // Image
        cell.catImage.image = UIImage(named: "cat\(indexPath.row)")
        
    return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    
    // ------------------------------------------------
    // MARK: - SELECT A CATEGORY
    // ------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCategory = "\(categoriesList[indexPath.row])"
        dismiss(animated: true, completion: nil)
    }
    
    
}// ./ end
