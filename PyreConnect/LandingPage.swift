//
//  LandingPage.swift
//  PyreConnect
//
//  Created by Oliwer Svensson on 2019-06-20.
//  Copyright © 2019 xscoder. All rights reserved.
//

import UIKit
import Parse

class LandingPage: UIViewController {

    @IBOutlet weak var exitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func exitPage(_ sender: Any) {
        
        let currentUser = PFUser.current()!
        var hasSeenNews = currentUser[USER_HAS_SEEN_NEWS]
        
        // Prepare data
        hasSeenNews = true
        currentUser[USER_HAS_SEEN_NEWS] = hasSeenNews
        
        // Saving block
        currentUser.saveInBackground(block: { (succ, error) in
            if error == nil {
                self.hideHUD()
                // error
            } else { self.hideHUD();
            }})
        
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
