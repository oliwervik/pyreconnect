// REPORT A USER  ----------------------------------------
Parse.Cloud.define("reportUser", function(request, response) {

    var userId = request.params.userId;
    var reportedBy = request.params.reportedBy;

    // Query
    var User = Parse.Object.extend('_User'),
    user = new User({ objectId: userId });
    user.set('reportedBy', reportedBy);

    Parse.Cloud.useMasterKey();
    user.save(null, { useMasterKey: true } ).then(function(user) {
        response.success(user);
    }, function(error) {
        response.error(error)
    });
});


// FAVORITE A USER  ----------------------------------------
Parse.Cloud.define("favoriteUser", function(request, response) {

    var userId = request.params.userId;
    var favoritedBy = request.params.favoritedBy;

    // Query
    var User = Parse.Object.extend('_User'),
    user = new User({ objectId: userId });
    user.set('favoritedBy', favoritedBy);

    Parse.Cloud.useMasterKey();
    user.save(null, { useMasterKey: true } ).then(function(user) {
        response.success(user);
    }, function(error) {
        response.error(error)
    });
});


// SEND iOS PUSH NOTIFICATION ---------------------------
Parse.Cloud.define("pushiOS", function(request, response) {

  var user = request.user;
  var params = request.params;
  var userObjectID = params.userObjectID
  var data = params.data

  var recipientUser = new Parse.User();
  recipientUser.id = userObjectID;

  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo("userID", userObjectID);

  Parse.Push.send({
    where: pushQuery,
    data: data
  }, { success: function() {
      console.log("#### PUSH SENT!");
  }, error: function(error) {
      console.log("#### PUSH ERROR: " + error.message);
  }, useMasterKey: true});
  response.success('success');
});


// SEND PUSH NOTIFICATION FOR ANDROID
Parse.Cloud.define("pushAndroid", function(request, response) {

  var user = request.user;
  var params = request.params;
  var userObjectID = params.userObjectID;
  var data = params.data;

  
  var recipientUser = new Parse.User();
  recipientUser.id = userObjectID;

  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo("userID", userObjectID);


  Parse.Push.send({
    where: pushQuery, 
    data: {
       alert: data
    }  
	}, { success: function() {
      console.log("#### PUSH OK");
  	}, error: function(error) {
      console.log("#### PUSH ERROR" + error.message);
  	}, useMasterKey: true });

  	response.success('success');
});

